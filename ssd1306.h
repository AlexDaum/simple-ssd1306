/*
 * oled.h
 *
 *  Created on: 30.10.2018
 *      Author: alex
 */

#ifndef SSD1306_H_
#define SSD1306_H_

#include "ssd1306_defs.h"
//this must be here, because it needs disp_t
#include "FontTable.h"

struct disp_rect {
	disp_t x, y, width, height;
	disp_rect(disp_t x, disp_t y, disp_t width, disp_t height) :
			x(x), y(y), width(width), height(height) {
	}
};

struct controlByte {
	uint8_t byte;
	controlByte(bool data_only, bool isNextCommand) {
		byte = 0;
		if (!data_only) byte |= 0x80;
		if (!isNextCommand) byte |= 0x40;
	}

};

/*=====================================================
 * 					MAIN CLASS
 =====================================================*/

class SSD1306 {
public:
	SSD1306();
	~SSD1306();

	void init(disp_t brightness);
	void refresh();
	void refresh(disp_t bank);
	void clear();
	void writePixel(disp_t x, disp_t y, bool color);
	void setPixel(disp_t x, disp_t y);
	void clearPixel(disp_t x, disp_t y);
	void fillRect(const disp_rect &r);
	void clearRect(const disp_rect &r);

	void writePart(const uint8_t * const data, disp_rect bounds);
	void print(const char *text);
	void write(char c);
	void writeBreak(char c);
	void setCursor(disp_t row, disp_t column);
#ifdef STDLIB_ARRAY
	void writeSection(const std::array<uint8_t, WIDTH> &section, disp_t n);
	void writeWholeDisplay(
			const std::array<std::array<uint8_t, WIDTH>, PAGES> &display);
#endif
	//Write the display, the array MUST contain WIDTH elements
	void writeSection(const uint8_t * const section, disp_t n);
	//Write the display, the array MUST contain WIDTH*BANKS elements
	void writeWholeDisplay(const uint8_t * const display);

protected:
#ifdef STDLIB_ARRAY
	std::array<std::array<uint8_t, WIDTH>, PAGES> display_buffer;
#else
	uint8_t display_buffer[PAGES][WIDTH];
#endif
	void setPageAndColumn(disp_t page, disp_t column);
	disp_t cursorCol = 0, cursorRow = 0;
};

#endif /* SSD1306_H_ */
