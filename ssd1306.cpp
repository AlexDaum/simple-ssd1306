#include "ssd1306.h"
//need this for memcpy and memset
#include <string.h>
#include "FontTable.h"

FontTable fontTable;

SSD1306::SSD1306() {

}

SSD1306::~SSD1306() {

}

void SSD1306::init(disp_t brightness) {
	i2c_beginWrite(SSD_ADDRESS);
	//set brightness
	i2c_sendByte(controlByte(false, true).byte);
	i2c_sendByte(0x81);
	i2c_sendByte(controlByte(false, true).byte);
	i2c_sendByte(brightness);

	//enable display
	i2c_sendByte(controlByte(false, true).byte);
	i2c_sendByte(0xAF);
	i2c_wfw();
	i2c_end();
}

void SSD1306::refresh() {
	for (disp_t i = 0; i < PAGES; i++) {
		refresh(i);
	}
}

void SSD1306::setPageAndColumn(disp_t page, disp_t col) {
	col += COLUMN_OFFSET;
	i2c_beginWrite(SSD_ADDRESS);
	i2c_sendByte(controlByte(false, true).byte);
	i2c_sendByte(0xB0 | (page & 0x7));
	i2c_sendByte(controlByte(false, true).byte);
	i2c_sendByte(col & 0xF);
	i2c_sendByte(controlByte(false, true).byte);
	i2c_sendByte(0x10 | ((col >> 4) & 0xF));
	i2c_wfw();
	i2c_end();
}

void SSD1306::refresh(disp_t page) {

	setPageAndColumn(page, 0);

	i2c_beginWrite(SSD_ADDRESS);
	i2c_sendByte(controlByte(true, false).byte);
	for (disp_t i = 0; i < WIDTH; i++) {
		i2c_sendByte(display_buffer[page][i]);
	}
	i2c_wfw();
	i2c_end();
}

void SSD1306::clear() {
//	memset(&display_buffer, 0, WIDTH * PAGES);
  for(uint8_t i = 0; i < PAGES; i++){
    for(uint8_t j = 0; j < WIDTH; j++){
      display_buffer[i][j] = 0;
    }
  }
	refresh();
}

void SSD1306::writePixel(disp_t x, disp_t y, bool color) {
	disp_t page = y / 8;
	setPageAndColumn(page, x);
	if (color) display_buffer[page][x] |= 1 << (y % 8);
	else display_buffer[page][x] &= ~(1 << (y % 8));

	i2c_beginWrite(SSD_ADDRESS);
	i2c_sendByte(controlByte(true, false).byte);
	i2c_sendByte(display_buffer[page][x]);
	i2c_wfw();
	i2c_end();
}
void SSD1306::setPixel(disp_t x, disp_t y) {
	writePixel(x, y, true);
}
void SSD1306::clearPixel(disp_t x, disp_t y) {
	writePixel(x, y, false);
}

void SSD1306::fillRect(const disp_rect &r) {
	disp_t height = r.height;
	disp_t page_upper = r.y / 8;
	disp_t page_lower = (r.y + r.height - 1) / 8;
	disp_t y_in_page = r.y % 8;
	disp_t height_in_page = y_in_page + r.height > 8 ? 8 - y_in_page : r.height;
	uint8_t page_mask = 0xFF >> (8 - height_in_page - y_in_page);
	page_mask &= ~(0xFF >> (8 - y_in_page));

	do {
		setPageAndColumn(page_upper, r.x);
		i2c_beginWrite(SSD_ADDRESS);
		i2c_sendByte(controlByte(true, false).byte);
		for (disp_t i = r.x; i < r.x + r.width; i++) {
			display_buffer[page_upper][i] |= page_mask;
			i2c_sendByte(display_buffer[page_upper][i]);
		}
		i2c_wfw();
		i2c_end();
		page_upper++;
		y_in_page = 0;
		height -= height_in_page;
		height_in_page = height > 8 ? 8 : height;
		page_mask = 0xFF >> (8 - height_in_page - y_in_page);
		page_mask &= ~(0xFF >> (8 - y_in_page));
	} while (page_upper <= page_lower);
}
void SSD1306::clearRect(const disp_rect &r) {
	disp_t page_upper = r.y / 8;
	disp_t page_lower = (r.y + r.height - 1) / 8;
	disp_t y_in_page = r.y % 8;
	uint8_t page_mask = (1 << y_in_page) - 1;
	if (page_lower == page_upper)
		page_mask &= (1 << (8 - y_in_page - r.height)) - 1;
	page_mask = ~page_mask;

	setPageAndColumn(page_upper, r.x);
	i2c_beginWrite(SSD_ADDRESS);
	i2c_sendByte(controlByte(true, false).byte);
	for (disp_t i = r.x; i < r.x + r.width; i++) {
		display_buffer[page_upper][i] &= page_mask;
		i2c_sendByte(display_buffer[page_upper][i]);
	}
	i2c_wfw();
	i2c_end();

	if (page_upper != page_lower) {
		disp_rect r2(r.x, r.y + y_in_page, r.width, r.height - y_in_page);
		clearRect(r);
	}
}

void SSD1306::print(const char *text) {
	for (const char *c = text; *c; c++) {
		write(*c);
	}
}

void SSD1306::write(char c) {
	const Font f = fontTable.getFont(c);
	//only font with width <= 8 supported
	int offset = (8 - f.height) / 2;

	setPageAndColumn(cursorRow, cursorCol);
	i2c_beginWrite(SSD_ADDRESS);
	i2c_sendByte(controlByte(true, false).byte);
	for (disp_t i = 0; i < f.width; i++) {
#ifdef AVR
    uint8_t col = pgm_read_byte(f.shape + i);
#else
    uint8_t col = f.shape[i];
#endif
		display_buffer[cursorRow][i + cursorCol] = col << offset;
		i2c_sendByte(display_buffer[cursorRow][i + cursorCol]);
	}
	i2c_wfw();
	i2c_end();
	cursorCol += f.width + 1;
}

void SSD1306::writeBreak(char c) {
	write(c);
	//5 is font width
	if (cursorCol + 5 >= WIDTH) {
		cursorCol = 0;
		cursorRow++;
	}
	if (cursorRow >= PAGES) {
		cursorRow = 0;
	}
}

void SSD1306::setCursor(disp_t row, disp_t column) {
	cursorCol = column;
	cursorRow = row;
}

//FIXME
void SSD1306::writePart(const uint8_t * const data, disp_rect r) {
	disp_t height = r.height;
	disp_t page_upper = r.y / 8;
	disp_t page_lower = (r.y + r.height - 1) / 8;
	disp_t y_in_page = r.y % 8;
	disp_t height_in_page = y_in_page + r.height > 8 ? 8 - y_in_page : r.height;
	uint8_t page_mask = 0xFF >> (8 - height_in_page - y_in_page);
	page_mask &= ~(0xFF >> (8 - y_in_page));

	disp_t j = 0;

	do {
		setPageAndColumn(page_upper, r.x);
		i2c_beginWrite(SSD_ADDRESS);
		i2c_sendByte(controlByte(true, false).byte);
		for (disp_t i = r.x; i < r.x + r.width; i++) {
			display_buffer[page_upper][i] &= ~page_mask;
			display_buffer[page_upper][i] |= (page_mask & data[j++]);
			i2c_sendByte(display_buffer[page_upper][i]);
		}
		i2c_wfw();
		i2c_end();
		page_upper++;
		y_in_page = 0;
		height -= height_in_page;
		height_in_page = height > 8 ? 8 : height;
		page_mask = 0xFF >> (8 - height_in_page - y_in_page);
		page_mask &= ~(0xFF >> (8 - y_in_page));
	} while (page_upper <= page_lower);

}

#ifdef STDLIB_ARRAY
void SSD1306::writeSection(const std::array<uint8_t, WIDTH> &section,
		disp_t n) {
	std::copy(section.begin(), section.end(), this->display_buffer[n].begin());
	refresh(n);
}
void SSD1306::writeWholeDisplay(
		const std::array<std::array<uint8_t, WIDTH>, PAGES> &display) {
	std::copy(display.begin(), display.end(), this->display_buffer.begin());
	refresh();
}
#endif
//Write the display, the array MUST contain WIDTH elements
void SSD1306::writeSection(const uint8_t * const section, disp_t n) {
#ifdef STDLIB_ARRAY
	memcpy(&display_buffer[n], section, WIDTH);
#else
	memcpy(&display_buffer[n], section, WIDTH);
#endif
	refresh(n);
}
//Write the display, the array MUST contain WIDTH*BANKS elements
void SSD1306::writeWholeDisplay(const uint8_t * const display) {
#ifdef STDLIB_ARRAY
	memcpy(&display_buffer, display, WIDTH * PAGES);
#else
	memcpy(&display_buffer, display, WIDTH * PAGES);
#endif
	refresh();
}
