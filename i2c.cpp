#include "i2c.h"
#include <Arduino.h>

#include "avr/io.h"
#include "util/twi.h"

void i2c_init() {
  TWBR = 144;
}

void i2c_start() {
	TWCR = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN);
  while  (!(TWCR & (1<<TWINT)));
}

static int i2c_sendAddress(uint8_t addr){
  if ((TWSR & 0xF8) != TW_START)
     return -1;
  TWDR = addr;
  TWCR = (1<<TWINT) | (1<<TWEN);
  while (!(TWCR & (1<<TWINT)));
  return 0;
}

int i2c_beginRead(uint8_t addr){
  i2c_start();
  i2c_sendAddress(addr << 1 | 1);
}
int i2c_beginWrite(uint8_t addr){
  i2c_start();
  i2c_sendAddress(addr << 1);
}
uint8_t i2c_recvByte(int ack) {
   while (!(TWCR & (1<<TWINT)));
   uint8_t ret = TWDR;
   TWCR = (1<<TWINT) | (1<<TWEN);
   return ret;
}

int i2c_sendByte(uint8_t data) {
  while (!(TWCR & (1<<TWINT)));
  TWDR = data;
  TWCR = (1<<TWINT) | (1<<TWEN);
	return 0;
}

void i2c_wfw() {
  while (!(TWCR & (1<<TWINT)));
} 

void i2c_end() {
	TWCR = (1<<TWINT) | (1<<TWEN)| (1<<TWSTO);
  delay(1);
  TWCR = 0;
}
