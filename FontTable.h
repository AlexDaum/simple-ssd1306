/*
 * Font.h
 *
 *  Created on: 31.10.2018
 *      Author: alex
 */

#ifndef FONTTABLE_H_
#define FONTTABLE_H_

#include "ssd1306_defs.h"

struct Font {
	const uint8_t * const shape;
	const disp_t width, height;

	Font(const uint8_t *shape, disp_t wid, disp_t hei) :
			shape(shape), width(wid), height(hei) {
	}
};

class FontTable {
public:
	FontTable();
	~FontTable();

	const Font getFont(char ch);
};

#endif /* FONTTABLE_H_ */
