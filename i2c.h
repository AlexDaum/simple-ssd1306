/*
 * i2c.h
 *
 *  Created on: 04.08.2018
 *      Author: alex
 */

#ifndef I2C_H_
#define I2C_H_

#include <stdint.h>

void i2c_init();
int i2c_beginRead(uint8_t addr);
int i2c_beginWrite(uint8_t addr);
uint8_t i2c_recvByte(int ack);
int i2c_sendByte(uint8_t data);
void i2c_wfw();
void i2c_end();

#endif /* I2C_H_ */
