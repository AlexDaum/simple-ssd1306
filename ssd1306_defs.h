/*
 * ssd1306_defs.h
 *
 *  Created on: 31.10.2018
 *      Author: alex
 */

#ifndef SSD1306_DEFS_H_
#define SSD1306_DEFS_H_
/*=====================================================
 * 					DEFINE SECTION
 =====================================================*/

//Define the Address in 8bit Mode
#define SSD_ADDRESS 0x3C

//If your Display has an offset (the first column is in fact not address 0)
//Put the real address of the first column here
#define COLUMN_OFFSET 2

//Define the resolution of the Display
#define WIDTH 128
#define HEIGHT 64

//The amount of banks, always HEIGHT/8
#define PAGES HEIGHT/8

//Enable use of std::array
//#define STDLIB_ARRAY

//uncomment this line if you do not have enough SRAM to keep the whole buffer (~1kB)
//TODO not implemented!
//#define LOCAL_BUFFER

//define this for AVRs, in ifdef, because e.g. Arduino defines this already
#ifndef AVR
#define AVR
#endif

/*=====================================================
 * 					INCLUDE SECTION
 =====================================================*/

#ifdef STDLIB_ARRAY
#include <array>
#endif

/*
 * You have to create the methods declared in i2c.h, so that
 * they work on the particular device, if they are not supplied
 */
#include "i2c.h"

#ifdef AVR
#include <avr/pgmspace.h>
#endif


/*=====================================================
 * 					TYPEDEFS SECTION
 =====================================================*/
/*
 * Set this to the preffered type for indexing and counting. Has to be able to represent
 * At least max(WIDTH, HEIGHT, 255). On 8-bit controllers, you should set this to uint8_t,
 * on 32bit controllers set this as int.
 */
typedef uint8_t disp_t;
#ifdef AVR
#define CONSTANT const PROGMEM
#else
#define CONSTANT const
#endif

#endif /* SSD1306_DEFS_H_ */
